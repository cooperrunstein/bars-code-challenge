import { Color } from "csstype";

export type SingleBarData = {
  title: string;
  left: PartialBarType;
  right: PartialBarType;
};

export type PartialBarType = {
  color: Color;
  value: number;
};

export enum Side {
  LEFT = "left",
  RIGHT = "right"
}

export type Percents = { rightPercent: number; leftPercent: number };

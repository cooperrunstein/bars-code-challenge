import { apiParser, applyColors } from "../utils";

import { Side, SingleBarData } from "../typings/globalTypes";

export const getAndParseApiData = async (URL: string) => {
  const data: Response = await fetch(URL);
  const json: object[] = await data.json();
  const parsedData: SingleBarData[] = apiParser(json).map(d =>
    applyColors(d, "#4286f4", "#ba3995")
  );
  return parsedData;
};

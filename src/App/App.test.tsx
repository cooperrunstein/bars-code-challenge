import "jest-dom/extend-expect";
import * as React from "react";

import { render, wait } from "react-testing-library";

import App from "./App";

jest.mock("./fetchHelper", () => {
  return {
    getAndParseApiData: () => [
      /*tslint:disable*/
      {
        title: "Data1",
        left: { color: "#007cff", value: 10 },
        right: { color: "#ffe944", value: 30 }
      },
      {
        title: "Data2",
        left: { color: "#007cff", value: 36 },
        right: { color: "#ffe944", value: 85 }
      },
      {
        title: "Data3",
        left: { color: "#007cff", value: 79 },
        right: { color: "#ffe944", value: 52 }
      }
    ]
  };
});

describe("App", () => {
  it("displays the correct title", () => {
    const { getByText, getByTestId } = render(<App />);
    expect(getByText("Bunch of Bars"));
    expect(getByTestId("title"));
  });

  it("displays a loader", () => {
    const { getByText } = render(<App />);
    const loader = getByText("Loading...");
    expect(loader);
  });

  it("displays the data after loading from api", async () => {
    const { getByText } = render(<App />);
    await wait(() => expect(getByText("25%")));
  });
});

import * as React from "react";

import DataPointGraph from "../components/DataPointDisplay";
import MultiBar from "../components/MultiBar";

import { Side, SingleBarData } from "../typings/globalTypes";

import styles from "./App.module.css";
import { getAndParseApiData } from "./fetchHelper";

const { useState, useEffect } = React;

const URL =
  "https://gist.githubusercontent.com/gargrave/e2fd3d07d44862a094dabb36137a9187/raw/29f8aef5813e1f67ab12f90617638091561b6b25/mock-api.json";

type ActiveDataPoint = {
  idx: number;
  side: Side;
};

const App: React.FunctionComponent = () => {
  const [activeDataPoint, setActiveDataPoint] = useState<ActiveDataPoint>({
    idx: -1,
    side: Side.LEFT
  });
  const [clicked, setClicked] = useState<boolean>(false);
  const [data, setData] = useState<SingleBarData[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  const setInitialData = async () => {
    const parsedData: SingleBarData[] = await getAndParseApiData(URL);
    setData(parsedData);
    setLoading(false);
  };

  const stickExpandedData = (idx: number, side: Side) => {
    setClicked(true);
  };

  const selectExpandedData = (idx: number, active: boolean, side: Side) => {
    if (active) {
      handleMouseEnterBar(idx, side);
    } else {
      handleMouseLeaveBar(idx, side);
    }
  };

  const handleMouseEnterBar = (idx: number, side: Side) => {
    setActiveDataPoint({ idx, side });
    setClicked(false);
  };

  const handleMouseLeaveBar = (idx: number, side: Side) => {
    if (!clicked) {
      setActiveDataPoint({ idx: -1, side });
    }
  };

  const checkShouldCloseData = () => {
    if (clicked) {
      setActiveDataPoint({
        idx: -1,
        side: Side.LEFT
      });
    }
  };

  const incomingDataCurrentInState = (idx: number, side: Side) => {
    return activeDataPoint.idx === idx && activeDataPoint.side === side;
  };

  const currentId = activeDataPoint.idx;

  useEffect(() => {
    setInitialData();
  }, []);

  return (
    <div className={styles.app} onClick={checkShouldCloseData}>
      <h1 data-testid={"title"}>Bunch of Bars</h1>
      {loading && <h1>Loading...</h1>}
      {!loading && (
        <div className={styles.wrapper}>
          <MultiBar
            values={data}
            mouseOverCallback={selectExpandedData}
            mouseClickCallback={stickExpandedData}
          />
          {activeDataPoint.idx !== -1 && data.length > 0 && (
            <DataPointGraph
              value={data[currentId][activeDataPoint.side].value}
              title={data[currentId].title}
              total={data[currentId].left.value + data[currentId].right.value}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default App;

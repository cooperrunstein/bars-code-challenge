import * as utils from "./utils";

import { cleanup } from "react-testing-library";

describe("utils", () => {
  beforeEach(() => {
    cleanup();
  });
  describe("hex to rgb", () => {
    it("converts correctly", () => {
      expect(utils.hexToRgb("#ffe944")).toBe("rgb(255, 233, 68)");
    });
  });

  describe("left and right percentages", () => {
    it("converts correctly", () => {
      expect(utils.leftRightPercents(4, 6).rightPercent).toBe(60);
      expect(utils.leftRightPercents(4, 6).leftPercent).toBe(40);
    });
  });

  describe("apiParser", () => {
    let mockData: Array<{}>;
    beforeEach(() => {
      mockData = [
        {
          /*tslint:disable*/
          name: "Tasks and stuff",
          incomplete: 60,
          complete: 241
        },
        {
          name: "Sub-Tasks and stuff",
          incomplete: 10,
          complete: 40
        },
        {
          low_prio: 50,
          name: "Bug Reports and stuff",
          high_prio: 76
        }
      ];
    });
    it("parses mock json", () => {
      const parsedData = utils.apiParser(mockData);
      expect(parsedData[0].title).toBe("Tasks and stuff");
      expect(parsedData[0].left.value).toBe(241);
      expect(parsedData[0].right.value).toBe(60);
    });
  });

  describe("applyColors", () => {
    it("applys the correct colors", () => {
      const originalData = {
        title: "title",
        left: {
          value: 50
        },
        right: {
          value: 60
        }
      };
      const withColor = utils.applyColors(originalData, "#eee", "#000");
      expect(withColor.left.color).toBe("#eee");
      expect(withColor.right.color).toBe("#000");
    });
  });
});

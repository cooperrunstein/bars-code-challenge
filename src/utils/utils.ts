import { Color } from "csstype";
import { Percents, SingleBarData } from "../typings/globalTypes";

export const hexToRgb = (hex: string): string => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  type rgbType = {
    r: number;
    g: number;
    b: number;
  };

  const rgb: rgbType | null = result
    ? {
        /* tslint:disable */
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null;

  if (rgb) {
    return `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`;
  }
  return "";
};

export const leftRightPercents = (
  leftVal: number,
  rightVal: number
): Percents => {
  const leftFloat = (leftVal / (leftVal + rightVal)) * 100;
  const leftPercent = Math.round(leftFloat);
  const rightPercent = 100 - leftPercent;
  return { rightPercent, leftPercent };
};

export type UnfilledBarProps = {
  title: string;
  left: {
    value: number;
  };
  right: {
    value: number;
  };
};

function notNull<T>(value: T | null): value is T {
  return value !== null;
}

export const apiParser = (data: Object[]): UnfilledBarProps[] => {
  return data
    .filter(entry => Object.keys(entry).length === 3)
    .map(apiDataPointParse)
    .filter(notNull);
};

const apiDataPointParse = (dataPoint: Object): UnfilledBarProps | null => {
  let title = null;
  let left = null;
  let right = null;

  for (let [key, value] of Object.entries(dataPoint)) {
    if (typeof value == "string") {
      title = value;
    } else {
      right ? (left = value) : (right = value);
    }
  }
  if (title !== null && right !== null && left !== null) {
    const result: UnfilledBarProps = {
      title,
      left: {
        value: left
      },
      right: {
        value: right
      }
    };
    return result;
  }
  return null;
};

export const applyColors = (
  data: UnfilledBarProps,
  left: Color,
  right: Color
): SingleBarData => {
  return {
    ...data,
    left: {
      ...data.left,
      color: left
    },
    right: {
      ...data.right,
      color: right
    }
  };
};

import * as React from "react";

import styles from "./DataPointDisplay.module.css";

export type GraphDataPointProps = {
  title: string;
  value: number;
  total: number;
};

const GraphDataPoint: React.SFC<GraphDataPointProps> = ({
  title,
  total,
  value
}) => {
  return (
    <div className={styles.wrapper}>
      <h3>{title}</h3>
      <p>
        Value: {value} ( {Math.round((value / total) * 100)}%)
      </p>
      <p>Total: {total}</p>
    </div>
  );
};

export default React.memo(GraphDataPoint);

import "jest-dom/extend-expect";
import * as React from "react";
import { cleanup, fireEvent, render } from "react-testing-library";
import { hexToRgb, leftRightPercents } from "../../utils";

import { Side } from "../../typings/globalTypes";

import SingleBar, { SingleBarProps } from "./SingleBar";

describe("Single Bar", () => {
  let defaultProps: SingleBarProps;
  let moCb: () => void;
  let mcCb: () => void;
  afterEach(() => {
    cleanup();
  });

  beforeEach(() => {
    mcCb = jest.fn();
    moCb = jest.fn();
    defaultProps = {
      left: { color: "#007cff", value: 48 },
      mouseClickCallback: mcCb,
      mouseOverCallback: moCb,
      right: { color: "#ffe944", value: 272 },
      title: "Tasks Completed"
    };
  });

  describe("Display", () => {
    it("displayed the correct title and correct total", () => {
      const left = defaultProps.left.value;
      const right = defaultProps.right.value;
      const total = (left + right).toString();
      const { getByText } = render(<SingleBar {...defaultProps} />);

      expect(getByText(defaultProps.title));
      expect(getByText(`Total: ${total}`));
    });

    it("displays the correct colors", () => {
      const { getByTestId } = render(<SingleBar {...defaultProps} />);

      // If for some reason the children don't fill the space, the bar won't look broken
      expect(getByTestId("parentBar").style.backgroundColor).toBe(
        hexToRgb(defaultProps.right.color) || defaultProps.left.color
      );
      expect(getByTestId("childLeftBar").style.backgroundColor).toBe(
        hexToRgb(defaultProps.left.color) || defaultProps.left.color
      );

      expect(getByTestId("childRightBar").style.backgroundColor).toBe(
        hexToRgb(defaultProps.right.color) || defaultProps.right.color
      );
    });

    it("displays the percentages", () => {
      const { getByText } = render(<SingleBar {...defaultProps} />);
      const { leftPercent, rightPercent } = leftRightPercents(
        defaultProps.left.value,
        defaultProps.right.value
      );
      expect(getByText(`${rightPercent}%`));
      expect(getByText(`${leftPercent}%`));
    });
  });

  describe("Functionality", () => {
    it("calls the callback on mouseover", () => {
      const { getByTestId } = render(<SingleBar {...defaultProps} />);
      fireEvent.mouseOver(getByTestId("childLeftBar"));
      expect(moCb).toBeCalledWith(true, Side.LEFT);
    });
    it("calls the callback on mouseoff", () => {
      const { getByTestId } = render(<SingleBar {...defaultProps} />);
      fireEvent.mouseOver(getByTestId("childLeftBar"));
      fireEvent.mouseLeave(getByTestId("childLeftBar"));
      expect(moCb).toBeCalledWith(false, Side.LEFT);
    });
  });
});

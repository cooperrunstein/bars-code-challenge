import * as React from "react";

import styles from "./SingleBar.module.css";

import { Percents, Side, SingleBarData } from "../../typings/globalTypes";
import { leftRightPercents } from "../../utils";

export type SingleBarProps = SingleBarData & MouseEventCallbacks;

type MouseEventCallbacks = {
  mouseClickCallback: (side: Side) => void;
  mouseOverCallback: (active: boolean, side: Side) => void;
};

const SingleBar: React.SFC<SingleBarProps> = ({
  title,
  left,
  right,
  mouseClickCallback,
  mouseOverCallback
}) => {
  const { leftPercent, rightPercent }: Percents = leftRightPercents(
    left.value,
    right.value
  );

  const onMouseEvent = (active: boolean, side: Side, clicked: boolean) => () =>
    clicked ? mouseClickCallback(side) : mouseOverCallback(active, side);

  return (
    <div className={styles.container}>
      <div className={styles.titleBar}>
        <h3>{title}</h3>
        <p>Total: {left.value + right.value}</p>
      </div>
      <div
        data-testid="parentBar"
        style={{ backgroundColor: right.color }}
        className={styles.parentBar}
      >
        <div
          data-testid="childLeftBar"
          style={{ backgroundColor: left.color, width: `${leftPercent}%` }}
          className={styles.childLeftBar}
          onMouseEnter={onMouseEvent(true, Side.LEFT, false)}
          onMouseLeave={onMouseEvent(false, Side.LEFT, false)}
          onClick={onMouseEvent(true, Side.LEFT, true)}
        >
          <p>{`${leftPercent}%`}</p>
        </div>
        <div
          data-testid="childRightBar"
          className={styles.childRightBar}
          style={{
            backgroundColor: right.color,
            width: `${rightPercent}%`
          }}
          onMouseEnter={onMouseEvent(true, Side.RIGHT, false)}
          onMouseLeave={onMouseEvent(false, Side.RIGHT, false)}
          onClick={onMouseEvent(true, Side.RIGHT, true)}
        >
          <p>{`${rightPercent}%`}</p>
        </div>
      </div>
    </div>
  );
};

export default React.memo(SingleBar);

import * as React from "react";

import styles from "./MultiBar.module.css";

import SingleBar from "../SingleBar";

import { Side, SingleBarData } from "../../typings/globalTypes";

export type MultiBarProps = {
  values: SingleBarData[];
  mouseOverCallback: (id: number, active: boolean, side: Side) => void;
  mouseClickCallback: (id: number, side: Side) => void;
};

const mouseOverCallbackWithId = (
  index: number,
  cb: (id: number, active: boolean, side: Side) => void
) => (active: boolean, side: Side) => cb(index, active, side);

const mouseClickCallbackWithId = (
  index: number,
  cb: (id: number, side: Side) => void
) => (side: Side) => cb(index, side);

const MultiBar: React.SFC<MultiBarProps> = ({
  values,
  mouseOverCallback,
  mouseClickCallback
}) => {
  return (
    <div className={styles.container}>
      {values.map((data, index) => (
        <div className={styles.singleBarWrapper} key={index}>
          <SingleBar
            {...data}
            mouseOverCallback={mouseOverCallbackWithId(
              index,
              mouseOverCallback
            )}
            mouseClickCallback={mouseClickCallbackWithId(
              index,
              mouseClickCallback
            )}
          />
        </div>
      ))}
    </div>
  );
};

export default React.memo(MultiBar);

export { mouseOverCallbackWithId as _mouseOverCallbackWithIdForTesting };

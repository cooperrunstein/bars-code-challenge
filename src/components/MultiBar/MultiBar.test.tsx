import "jest-dom/extend-expect";
import * as React from "react";

import { cleanup, render } from "react-testing-library";

import MultiBar, {
  _mouseOverCallbackWithIdForTesting,
  MultiBarProps
} from "./MultiBar";

import { Side } from "../../typings/globalTypes";

describe("Multi Bar", () => {
  let defaultProps: MultiBarProps;
  const cbMo = jest.fn();
  const cbC = jest.fn();
  afterEach(() => {
    cleanup();
  });

  beforeEach(() => {
    defaultProps = {
      mouseClickCallback: cbC,
      mouseOverCallback: cbMo,

      /* tslint:disable */
      values: [
        {
          title: "Data1",
          left: { color: "#007cff", value: 48 },
          right: { color: "#ffe944", value: 240 }
        },
        {
          title: "Data2",
          left: { color: "#007cff", value: 36 },
          right: { color: "#ffe944", value: 85 }
        },
        {
          title: "Data3",
          left: { color: "#007cff", value: 79 },
          right: { color: "#ffe944", value: 52 }
        }
      ]
    };
  });

  it("displays all the titles", () => {
    const { getByText } = render(<MultiBar {...defaultProps} />);

    expect(getByText(defaultProps.values[0].title));
    expect(getByText(defaultProps.values[1].title));
    expect(getByText(defaultProps.values[2].title));
  });

  describe("Helper functions", () => {
    it("returns a function with the correct index", () => {
      const func = jest.fn();
      _mouseOverCallbackWithIdForTesting(4, func)(true, Side.LEFT);
      expect(func).toBeCalledWith(4, true, Side.LEFT);
    });
  });
});
